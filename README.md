# Сервис Writer

Сервис Writer принимает данные от сервиса Reader, обрабатывает их и сохраняет.

**_Внимание!_** Сервис Writer должен быть запущен раньше, чем сервис Reader.

## Работа с сервисов
Развертывание проекта
```bash
git clone https://gitlab.com/r-vision-test/writer.git
cd writer
npm install
npm run build
npm run start -- --nats --from 'nats://localhost:4222/' --to './example'
```

## Опции
### **--type**
Способ передачи данных.
* `--type web-socket` передача данных с использованием веб-сокета.
* `--type nats` передача данных с использованием NATS.
### **--web-socket** / **-ws**
Тоже, что `--type web-socket`.
### **--nats**
Тоже, что `--type nats`.
### **--from** / **-f**
Адрес удаленного ресурса, который будет отправлять данные.
### **--to** / **-t**
Место на локальной машине, куда будет сохранен полученный файл.