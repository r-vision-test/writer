import parseCommandLineArgs from "./parseCommandLineArgs";

it("Should return empty object", () => {
  const options = parseCommandLineArgs();
  expect(options).toEqual({});
});

it("Should return empty object with empty array as argument", () => {
  const options = parseCommandLineArgs([]);
  expect(options).toEqual({});
});

it("Should parse args with full name options", () => {
  const options = parseCommandLineArgs(["", "", "--type", "nats", "--from", "./text.txt", "--to", "nats://localhost:4222"]);
  expect(options).toEqual({
    type: "nats",
    from: "./text.txt",
    to: "nats://localhost:4222",
  });
});

it("Should parse args with short name options", () => {
  const options = parseCommandLineArgs(["", "", "-ws", "-f", "./text.txt", "-t", "ws://localhost:2222"]);
  expect(options).toEqual({
    type: "web-socket",
    from: "./text.txt",
    to: "ws://localhost:2222",
  });
});

it("Should parse args with nats options", () => {
  const options = parseCommandLineArgs(["", "", "--nats", "-f", "./text.txt", "-t", "nats://localhost:4222"]);
  expect(options).toEqual({
    type: "nats",
    from: "./text.txt",
    to: "nats://localhost:4222",
  });
});

it("Should parse args with web-socket options", () => {
  const options = parseCommandLineArgs(["", "", "--web-socket", "-f", "./text.txt", "-t", "ws://localhost:2222"]);
  expect(options).toEqual({
    type: "web-socket",
    from: "./text.txt",
    to: "ws://localhost:2222",
  });
});
