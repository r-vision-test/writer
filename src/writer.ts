import * as fs from "fs";
import * as stream from "stream";
import * as colors from "colors";
import parseCommandLineArgs from "./utils/parseCommandLineArgs";
import NatsReadStream from "./streams/NatsReadStream";
import RemoteReadStream from "./streams/RemoteReadStream";
import WebSocketReadStream from "./streams/WebSocketReadStream";

export default function writer(options = parseCommandLineArgs()) {
  const type = options.type;

  if (typeof type !== "string") {
    console.error(`${colors.red("[Error]")} Reader option 'type' must be a string`);
    return;
  }

  let readStreamClass: typeof RemoteReadStream | undefined;

  if (type === "web-socket") {
    readStreamClass = WebSocketReadStream;
  } else if (type === "nats") {
    readStreamClass = NatsReadStream;
  } else {
    console.error(`${colors.red("[Error]")} Read stream is undefined`);
    return;
  }

  const from = options.from;

  if (typeof from !== "string" && type !== "web-socket") {
    console.error(`${colors.red("[Error]")} Reader option 'from' must be a string`);
    return;
  }

  const to = options.to;

  if (typeof to !== "string") {
    console.error(`${colors.red("[Error]")} Reader option 'to' must be a string`);
    return;
  }

  readStreamClass.create(from, (createReadStreamError, readStream) => {

    if (createReadStreamError) {
      console.error(`${colors.red("[Error]")} ${createReadStreamError.message}`);
      return;
    }

    if (readStream) {
      const writeStream: fs.WriteStream & { _oldWrite?: (chunk: any, encoding: string, callback: (error?: Error | null | undefined) => void) => void }
        = fs.createWriteStream(to);

      writeStream._writev = undefined;

      writeStream._oldWrite = writeStream._write;

      writeStream._write
        = function (chunk: any, encoding: string, callback: (error?: Error | null | undefined) => void) {

          /**
           * Устанавливаем задержку в записи данных по условиям задания.
           */
          setTimeout(() => {
            if (typeof this._oldWrite === "function") {
              this._oldWrite(chunk, encoding, callback);
            }
          }, 500);
        };

      stream.pipeline(
        readStream,
        writeStream,
        (pipelineError) => {
          if (pipelineError) {
            console.error(`${colors.red("[Error]")} ${pipelineError.message}`);
            return;
          }

          console.log(`${colors.green("[Success]")} Downloading data from remote machine was successfully finished.`);
        }
      );
    }
  });
}
