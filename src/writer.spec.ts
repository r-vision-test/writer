import { EventEmitter } from "events";
import * as colors from "colors";

const hosts: { [host: string]: { incoming: EventEmitter; outgoing: EventEmitter } } = {};

const getEventEmitters = (host) => {
  if (typeof hosts[host] === "undefined") {
    hosts[host] = {
      incoming: new EventEmitter(),
      outgoing: new EventEmitter(),
    };
  }

  return hosts[host];
};

jest.mock("nats", () => ({
  connect: function (host: string) {
    if (host === "incorrect") {
      throw new Error("Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222");
    }

    const emitters = getEventEmitters(host);

    return {
      on: function (event, listener) { emitters.incoming.on(event, listener); return this; },
      request: function (event, ...args: any[]) { emitters.outgoing.emit(event, ...args); return this; },
      flush: function (listener) { emitters.outgoing.emit("flush", listener); return this; },
      close: function (listener) { emitters.outgoing.emit("close", listener); return this; },
      subscribe: function (name, listener) { emitters.incoming.on(name, listener); return this; },
      publish: function (...args: any[]) { emitters.outgoing.emit("publish", ...args); return this; },
    };
  },
}));

import writer from "./writer";

it("Should output error with empty options", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'type' must be a string`);
    done();
  });
  writer();
});

it("Should output error with unknown 'type' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Read stream is undefined`);
    done();
  });
  writer({
    type: "rabbitmq",
  });
});

it("Should output error with undefined 'from' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'from' must be a string`);
    done();
  });
  writer({
    type: "nats",
  });
});

it("Should output error with undefined 'to' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'to' must be a string`);
    done();
  });
  writer({
    type: "web-socket",
  });
});

it("Should output error with invalid NATS url", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222`);
    done();
  });

  writer({
    type: "nats",
    from: "incorrect",
    to: "./test",
  });
});

it("Should output error with invalid NATS url", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222`);
    done();
  });

  writer({
    type: "nats",
    from: "incorrect",
    to: "./test",
  });
});

it("Should output error due to writing data", done => {
  const host = "nats://localhost:4222";
  const emitters = getEventEmitters(host);

  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Some error`);
    done();
  });

  writer({
    type: "nats",
    from: host,
    to: "./test",
  });

  emitters.incoming.emit("connect");
  emitters.incoming.emit("error", new Error("Some error"));
});

it("Should successfully end writing data", done => {
  const host = "nats://localhost:4223";
  const emitters = getEventEmitters(host);

  console.log = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.green("[Success]")} Downloading data from remote machine was successfully finished.`);
    done();
  });

  writer({
    type: "nats",
    from: host,
    to: "./test",
  });

  emitters.incoming.emit("connect");
  emitters.incoming.emit("transferring", "", 1);
});


afterAll(() => {
  jest.clearAllMocks();
});
