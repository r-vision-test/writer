/**
 * Опции Writer'а.
 */
declare interface IWriterOptions {

  /**
   * Способ передачи данных.
   */
  type?: "web-socket" | "nats" | string;

  /**
   * Адрес удаленного ресурса, который будет передавать данные.
   */
  from?: string;

  /**
   * Место на локальной машине, куда будет сохранен полученный файл.
   */
  to?: string;
}
