import { EventEmitter } from "events";
import WebSocketReadStream from "./WebSocketReadStream";

const sockets: {
  [port: string]: {
    serverIncoming: EventEmitter;
    serverOutgoing: EventEmitter;
    clientIncoming: EventEmitter; clientOutgoing: EventEmitter;
  };
} = {};

const getEventEmitters = (port) => {
  if (typeof sockets[port] === "undefined") {
    sockets[port] = {
      serverIncoming: new EventEmitter(),
      serverOutgoing: new EventEmitter(),
      clientIncoming: new EventEmitter(),
      clientOutgoing: new EventEmitter(),
    };
  }

  return sockets[port];
};

const getWebSocketClient = (port) => {
  const emitters = getEventEmitters(port);

  return {
    on: function (event, listener) { emitters.clientIncoming.on(event, listener); return this; },
    send: function (listener) { emitters.clientOutgoing.emit("message", listener); return this; },
    terminate: function () { emitters.clientOutgoing.emit("terminate"); return this; },
  };
};

jest.mock("ws", () => ({
  Server: function Server({ port }) {
    const emitters = getEventEmitters(port);

    return {
      on: function (event, listener) { emitters.serverIncoming.on(event, listener); return this; },
      close: function (listener) { emitters.serverOutgoing.emit("close", listener); return this; },
    };
  },
}));

it("Should start server with default port", done => {
  const port = "8080";
  const emitters = getEventEmitters(port);

  WebSocketReadStream.create(undefined, (error) => {
    done();
  });

  emitters.serverIncoming.emit("listening");
});

it("Should throw error while starting server", done => {
  const port = "8081";
  const emitters = getEventEmitters(port);

  WebSocketReadStream.create(port, (error) => {
    expect(error?.message).toBe("Error while connecting to WebSocket");
    done();
  });

  emitters.serverIncoming.emit("error", new Error("Error while connecting to WebSocket"));
});

it("Should terminate second connection", done => {
  const port = "8082";
  const emitters = getEventEmitters(port);

  emitters.clientOutgoing.on("terminate", () => {
    done();
  });

  WebSocketReadStream.create(port, (error) => {
    expect(error?.message).toBe("Error while connecting to WebSocket");
    done();
  });

  const client1 = getWebSocketClient(port);

  emitters.serverIncoming.emit("connection", client1);

  const client2 = getWebSocketClient(port);

  emitters.serverIncoming.emit("connection", client2);
});

it("Should received message and send reply", done => {
  const port = "8083";
  const emitters = getEventEmitters(port);

  emitters.clientOutgoing.on("message", (reply) => {
    expect(reply).toBe("reply:1");
    done();
  });

  WebSocketReadStream.create(port, (createError, stream) => {
    stream?.on("data", () => undefined);
  });

  const client = getWebSocketClient(port);
  emitters.serverIncoming.emit("connection", client);

  emitters.clientIncoming.emit("message", Buffer.from("Test", "utf8"));
});

afterAll(() => {
  jest.clearAllMocks();
});
