import { Readable, ReadableOptions } from "stream";
import { ConnectionStatus } from "./ConnectionStatus";

/**
 * Поток чтения с удаленного ресурса.
 */
export default abstract class RemoteReadStream extends Readable {

  /**
   * Статус соединения с удаленным ресурсом.
   */
  protected status: ConnectionStatus = ConnectionStatus.NotConnected;

  /**
   * Стартует соединение с удаленным ресурсом.
   */
  protected abstract start: (host: any, callback: (error?: Error | null) => void) => void;

  /**
   * Останавливает соединение с удаленным ресурсом.
   */
  protected abstract stop: () => void;

  protected constructor(options?: ReadableOptions) {
    super(options);

    this.on("end", () => {
      this.stop();
    });
  }

  /**
   * Создает поток чтения с удаленного ресурса.
   */
  public static create(
    this,
    host: any,
    callback: (
      error?: Error | null,
      stream?: RemoteReadStream
    ) => void,
  ) {
    try {
      const stream = new this();

      stream.start(host, connectError => {
        if (connectError) {
          callback(connectError);
          return;
        }

        callback(null, stream);
      });

    } catch (error) {
      callback(error);
    }
  }

  /**
   * Функция запроса на чтение.
   */
  read = () => {
    if (!this.isPaused() && this.status === ConnectionStatus.Paused) {
      this.status = ConnectionStatus.Connected;
      this.resume();
    }

    return super.read();
  };

  /**
   * Функция запроса на чтение.
   * *Не используется, но требует реализацию для работы.*
   */
  _read = () => undefined;

  /**
   * Ставит чтение на запись.
   */
  pause = () => {
    this.status = ConnectionStatus.Paused;
    return super.pause();
  };

  /**
   * Убирает паузу.
   */
  resume = () => {
    this.emit("resume");
    return super.resume();
  };

  /**
   * Обрабатывает сообщения с удаленного ресурса.
   * @param chunk Часть данных.
   * @param callback Срабатывает после того, как данные будут обработаны.
   */
  protected receive = (chunk: any, callback: () => void) => {
    const write = () => {
      this.push(chunk, "buffer");
      callback();
    };

    if (this.isPaused()) {
      this.once("resume", () => {
        write();
      });
      return;
    }

    write();
  };
}
