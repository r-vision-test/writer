import * as WebSocket from "ws";
import RemoteReadStream from "./RemoteReadStream";
import { ConnectionStatus } from "./ConnectionStatus";

/**
 * Поток чтения с удаленного ресурса по средствам WebSocket.
 */
export default class WebSocketReadStream extends RemoteReadStream {

  /**
   * WebSocket сервер.
   */
  private webSocketServer: WebSocket.Server;

  /**
   * WebSocket клиент.
   */
  private webSocketClient: WebSocket;

  /**
   * Идентификационный номер последнего обработанного сообщения.
   */
  private messageIds = 0;

  protected constructor() {
    super();
  }

  /**
   * Создает поток чтения с удаленного ресурса по средствам WebSocket.
   * @param port Порт, на котором будет развернут сервер. По умолчанию: 8080.
   * @param callback Обработчик, который вызывается в случае ошибки или успешного создания потока.
   */
  public static create(
    this,
    port: number | string | undefined,
    callback: (
      error?: Error | null,
      stream?: RemoteReadStream
    ) => void,
  ) {
    return super.create(port, callback);
  }

  /**
   * Создает поток чтения с удаленного ресурса по средствам WebSocket.
   * @param host Порт, на котором будет развернут сервер. По умолчанию: 8080.
   * @param callback Обработчик, который вызывается в случае успешного соединения или ошибки.
   */
  protected start = (port: string | number | undefined, callback: (error?: Error | null) => void) => {
    this.webSocketServer = new WebSocket.Server({
      port: port ? parseInt(port.toString(), 10) : 8080,
    });

    this.webSocketServer.on("connection", (socket) => {

      if (typeof this.webSocketClient !== "undefined") {
        socket.terminate();
      }

      this.webSocketClient = socket;

      this.webSocketClient.on("error", (error) => {
        this.destroy(error);
      });

      this.webSocketClient.on("message", (data: Buffer) => {
        this.receive(data.length > 0 ? data : null, () => {
          this.webSocketClient.send(`reply:${++this.messageIds}`);
        });
      });
    });

    this.webSocketServer.on("error", (error) => {
      if (this.status === ConnectionStatus.NotConnected) {
        callback(error);
      } else {
        this.destroy(error);
      }
    });

    this.webSocketServer.on("close", () => {
      this.destroy();
    });

    this.webSocketServer.on("listening", () => {
      this.status = ConnectionStatus.Connected;
      callback();
    });
  };

  /**
   * Отключает поток от NATS сервера.
   */
  protected stop = () => {
    this.webSocketServer.close();
  };
}
