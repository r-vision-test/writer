import * as NATS from "nats";
import RemoteReadStream from "./RemoteReadStream";
import { ConnectionStatus } from "./ConnectionStatus";

/**
 * Поток чтения с удаленного ресурса через NATS.
 */
export default class NatsReadStream extends RemoteReadStream {

  /**
   * NATS Клиент.
   */
  private nc: NATS.Client;

  protected constructor() {
    super();
  }

  /**
   * Создает поток чтения с удаленного ресурса через NATS.
   * @param host Адрес удаленного ресурса.
   * @param callback Обработчик, который вызывается в случае ошибки или успешного создания потока.
   */
  public static create(
    this,
    host: string,
    callback: (
      error?: Error | null,
      stream?: RemoteReadStream
    ) => void,
  ) {
    return super.create(host, callback);
  }

  /**
   * Создает подключение к NATS серверу.
   * @param host Адрес NATS сервера.
   * @param callback Обработчик, который вызывается в случае успешного соединения или ошибки.
   */
  protected start = (host, callback: (error?: Error | null) => void) => {
    if (typeof host !== "string") {
      callback(
        new Error("NATS host must be a string")
      );
      return;
    }

    this.nc = NATS.connect(host);

    this.nc.on("error", error => {
      if (this.status === ConnectionStatus.NotConnected) {
        callback(error);
      } else {
        this.destroy(error);
      }

      this.status = ConnectionStatus.Error;
    });

    this.nc.on("disconnect", () => {
      this.destroy(new Error("NATS disconnect"));
      this.status = ConnectionStatus.Closed;
    });

    this.nc.on("close", () => {
      this.destroy();
      this.status = ConnectionStatus.Closed;
    });

    this.nc.subscribe("transferring", (msg, reply) => {
      if (msg === "") {
        msg = null;
      } else {
        msg = Buffer.from(msg, "base64");
      }

      this.receive(msg, () => {
        this.nc.publish(reply);
      });
    });

    this.nc.on("connect", () => {
      this.status = ConnectionStatus.Connected;
      callback();
    });
  };

  /**
   * Отключает поток от NATS сервера.
   */
  protected stop = () => {
    this.nc.flush(() => {
      this.nc.close();
    });
  };
}
