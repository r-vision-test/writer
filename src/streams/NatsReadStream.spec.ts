import { EventEmitter } from "events";
import NatsReadStream from "./NatsReadStream";

const hosts: { [host: string]: { incoming: EventEmitter; outgoing: EventEmitter } } = {};

const getEventEmitters = (host) => {
  if (typeof hosts[host] === "undefined") {
    hosts[host] = {
      incoming: new EventEmitter(),
      outgoing: new EventEmitter(),
    };
  }

  return hosts[host];
};

jest.mock("nats", () => ({
  connect: function (host: string) {
    if (host === "incorrect") {
      throw new Error("Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222");
    }

    const emitters = getEventEmitters(host);

    return {
      on: function (event, listener) { emitters.incoming.on(event, listener); return this; },
      request: function (event, ...args: any[]) { emitters.outgoing.emit(event, ...args); return this; },
      flush: function (listener) { emitters.outgoing.emit("flush", listener); return this; },
      close: function (listener) { emitters.outgoing.emit("close", listener); return this; },
      subscribe: function (name, listener) { emitters.incoming.on(name, listener); return this; },
      publish: function (...args: any[]) { emitters.outgoing.emit("publish", ...args); return this; },
    };
  },
}));

it("Should throw error with empty options", done => {
  NatsReadStream.create(undefined as any, (error) => {
    expect(error?.message).toBe("NATS host must be a string");
    done();
  });
});

it("Should throw error while connecting to NATS", done => {
  const host = "nats://localhost:4222";
  const emitters = getEventEmitters(host);

  NatsReadStream.create(host, (error) => {
    expect(error?.message).toBe("Error while connecting to NATS");
    done();
  });

  emitters.incoming.emit("error", new Error("Error while connecting to NATS"));
});

it("Should throw error due to disconnecting", done => {
  const host = "nats://localhost:4223";
  const emitters = getEventEmitters(host);

  NatsReadStream.create(host, (createError, stream) => {
    stream?.on("error", (error) => {
      expect(error?.message).toBe("NATS disconnect");
      done();
    });

    emitters.incoming.emit("disconnect");
  });

  emitters.incoming.emit("connect");
});

it("Should close stream due to closing connection", done => {
  const host = "nats://localhost:4224/";
  const emitters = getEventEmitters(host);

  NatsReadStream.create(host, (createError, stream) => {
    stream?.on("close", () => {
      done();
    });
    emitters.incoming.emit("close");
  });

  emitters.incoming.emit("connect");
});

it("Should convert data from base64 to Buffer", done => {
  const host = "nats://localhost:4225/";
  const emitters = getEventEmitters(host);

  const buffer = Buffer.from("Test", "utf8");

  NatsReadStream.create(host, (createError, stream) => {
    stream?.on("data", (chunk: Buffer) => {
      expect(chunk).toEqual(buffer);
      done();
    });

    emitters.incoming.emit("transferring", buffer.toString("base64"), 1);
  });

  emitters.incoming.emit("connect");
});

it("Should convert data from base64 to Buffer", done => {
  const host = "nats://localhost:4226/";
  const emitters = getEventEmitters(host);

  const buffer = Buffer.from("Test", "utf8");

  NatsReadStream.create(host, (createError, stream) => {
    stream?.on("data", (chunk: Buffer) => {
      expect(chunk).toEqual(buffer);
      done();
    });

    emitters.incoming.emit("transferring", buffer.toString("base64"), 1);
  });

  emitters.incoming.emit("connect");
});

it("Should emit pause event", done => {
  const host = "nats://localhost:4227/";
  const emitters = getEventEmitters(host);

  NatsReadStream.create(host, (createError, stream) => {
    stream?.pause();
    const isPaused = stream?.isPaused();

    expect(isPaused).toBe(true);
    done();
  });

  emitters.incoming.emit("connect");
});

it("Should convert data from base64 to Buffer", done => {
  const host = "nats://localhost:4228/";
  const emitters = getEventEmitters(host);

  const buffer = Buffer.from("Test", "utf8");

  emitters.outgoing.on("publish", (reply) => {
    expect(reply).toBe(1);
    done();
  });

  NatsReadStream.create(host, (createError, stream) => {
    stream?.pause();

    emitters.incoming.emit("transferring", buffer.toString("base64"), 1);

    stream?.resume();
  });

  emitters.incoming.emit("connect");
});

it("Should close connection after sending data ending", done => {
  const host = "nats://localhost:4229/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("flush", (listener) => {
    listener();
  });

  emitters.outgoing.on("close", () => {
    done();
  });

  NatsReadStream.create(host, (createError, stream) => {
    stream?.emit("end");
  });

  emitters.incoming.emit("connect");
});
